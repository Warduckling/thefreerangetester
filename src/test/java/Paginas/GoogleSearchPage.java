package Paginas;

import org.junit.Assert;

public class GoogleSearchPage extends BasePage {

    private String searchBox = "//input[@title='Buscar']";
    private String searchButton = "//div[@class='FPdoLc VlcLAe']//input[@value='Buscar con Google']";

    public GoogleSearchPage () {
        super(driver);
    }

    public void navigateToGoogle(){
        navigateTo("https://www.google.com");
        Assert.assertEquals("Google",driver.getTitle());

    }

    public void enterSearchCriteria(String searchText){
        sendKeysTo(searchBox, searchText);
    }

    public void clickOnSearchGoogle(){
        clickElement(searchButton);
    }
}
