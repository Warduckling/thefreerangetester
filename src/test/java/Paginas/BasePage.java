package Paginas;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasePage {

    static Logger log = LoggerFactory.getLogger(BasePage.class);
    protected static WebDriver driver;
    private static WebDriverWait wait;

    static {
        ChromeOptions chromeOptions = new ChromeOptions();
        //chromeOptions.addArguments("--headless");
        driver = new ChromeDriver(chromeOptions);
        wait = new WebDriverWait(driver, 10);
    }

    public BasePage(WebDriver driver) {
        BasePage.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 10);
    }

    public static void closeBrowser(){
        driver.quit();
    }

    public static void navigateTo(String url){
        driver.get(url);
    }

    private WebElement Find(String locator)
    {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
    }

    public void clickElement(String locator)
    { Find(locator).click();}

    public void sendKeysTo(String locator, String keysToSend){
        Find(locator).clear();
        Find(locator).sendKeys(keysToSend);
    }

    public void isDisplayed(String locator){
        Assert.assertTrue(Find(locator).isDisplayed());

    }
    public void textIsCorrect(String locator, String text){
        Assert.assertEquals(text,Find(locator).getText());

    }

    public void selectFromDropdown(String locator, String value){
        Select dropDown = new Select (Find(locator));
        dropDown.selectByVisibleText(value);
    }

    public String getTextFromStaticTable(String locator, int row, int column)
    {
        String cellINeed = locator+"/table/tbody/tr["+row+"]/td["+column+"]";
        return Find(cellINeed).getText();
    }

    public void setValueOnStaticTable(String locator, int row, int column, String keysToCell)
    {
        String cellINeed = locator+"/table/tbody/tr["+row+"]/td["+column+"]";
        Find(cellINeed).sendKeys(keysToCell);
    }

    public void switchToIFrame(String frameId)
    {
        driver.switchTo().frame(frameId);
    }

    public void switchBacktoParentFrame()
    {
        driver.switchTo().parentFrame();
    }

    public void dismissAlert()
    {
        driver.switchTo().alert().dismiss();
    }
}

