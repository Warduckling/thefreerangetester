package Paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class EntrevistaAutomation {
    static WebDriver driver;
    public static void main(String[]args){
        driver = new ChromeDriver();
        driver.get("https://www.google.com/");
        boolean result;

        try{
            result= EntrevistaAutomation.clickLastResult();
        }
        catch(Exception e){
            e.printStackTrace();
            result=false;
        }

        finally{
            driver.close();
        }

        System.out.println("Test "+(result?"passed." : "failed"));

    }
    private static boolean clickLastResult() {
        driver.findElement(By.name("q")).sendKeys("qa automation\n");
        List<WebElement> links = driver.findElements(By.className("LC20lb"));
        WebElement lastLink = links.get(links.size()-1);
        lastLink.click();

        return true;
    }
}
