package Paginas;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class testNGTFRT  {

    public WebDriver driver;

    @BeforeMethod(alwaysRun = true)
    public void setupInicial() {
        System.out.println("Iniciamos ChromeDriver");
        driver = new ChromeDriver();
    }

    @Test(groups = "Smoke")
    public void pruebaTestNG(){
        driver.navigate().to("https://www.google.com");
        String titulo = driver.getTitle();
        Assert.assertEquals(titulo,"Bing");
    }

    @Test(groups = "NoCorrer")
    public void testNegativo(){
        driver.navigate().to("https://www.google.com");
        String titulo = driver.getTitle();
        Assert.assertEquals(titulo,"Google");
    }

    @AfterMethod(alwaysRun = true)
    public void limpiando(){
        System.out.println("Cerramos ChromeDriver");
        driver.quit();
    }

}
