package DefSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class APISteps {

    static Logger log = LoggerFactory.getLogger(APISteps.class);
    private ValidatableResponse json;
    private static RequestSpecification request;
    private Response response;

    @Given("^I send a request$")
    public void iAmOnTheGoogleSearchPage() {
    request = given().relaxedHTTPSValidation().log().all()
                     .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON);
    }

    @Then("^I get a list with (\\d+) users$")
    public void iGetAStatusCode(int expectedUserSize) {
        String endpoint = "https://jsonplaceholder.typicode.com/users";
        response = request.log().all().when()
                .get(endpoint);
        List<String> jsonResponse =response.jsonPath().getList("$");
        int actualUserSize = jsonResponse.size();
        assertEquals(expectedUserSize,actualUserSize);
    }

    @Then("^I get a list with the names$")
    public void iGetAllTheNames() {
        String endpoint = "https://jsonplaceholder.typicode.com/users";
        response = request.when()
                .get(endpoint);
        List<String> userNames = response.jsonPath().getList("username");
        System.out.println("The User is: "+userNames.get(0));
    }

    @Then("^I print the nested values$")
    public void printNested()
    {
        String json = response.getBody().asString();
        JSONObject obj = new JSONObject(json);
        JSONArray arr = obj.getJSONArray("Signatures");
        for(int i=0;i<arr.length();i++)
        {
            String nestedValue = arr.getJSONObject(i).getString("protected");
            log.info(nestedValue);
        }
    }
}

