package DefSteps;


import Paginas.BasePage;
import Paginas.GoogleSearchPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import groovy.util.logging.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

@Slf4j
public class MyStepdefs {
    static Logger log = LoggerFactory.getLogger(MyStepdefs.class);
    GoogleSearchPage googleSearch = new GoogleSearchPage();
    SoftAssert softAssert = new SoftAssert();


    @Given("^I am on the Google search page$")
    public void iAmOnTheGoogleSearchPage() {
    googleSearch.navigateToGoogle();
    }

    @When("^I enter a \"([^\"]*)\" in the search field$")
    public void iEnterAWordInTheSearchField(final String textoBusqueda) {
    googleSearch.enterSearchCriteria(textoBusqueda + "\n");
    }

    @And("^click on the search button$")
    public void clickOnTheSearchButton() {
        googleSearch.clickOnSearchGoogle();
    }


    @Then("^I see results related to the word entered$")
    public void iSeeResultsRelatedToTheWordEntered() {
        Assert.assertEquals("Hola", "Hola", "Primera validacion fallando!");
    }

    @And("^I close the browser$")
    public void iCloseTheBrowser() {
        BasePage.closeBrowser();
    }
}
