Feature: Ejemplo The Free Range Tester

Background:
  Given I am on the Google search page

  @CucumberTag
  Scenario Outline: Navigate to Google and Search a keyword.
    When I enter a "<word>" in the search field
    Then I see results related to the word entered


    Examples:
      |word|
      |Nicholas Cage|
      |Adam Sandler|
      |Brendan Fraser|

